﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.SystemRuntime;

namespace XXF.BaseService.ServiceCenter.Client.ClientPool
{
    /// <summary>
    /// 客户端配置
    /// </summary>
    public class BaseClientConfig
    {
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// 服务端口
        /// </summary>
        public int Port { get; set; }

        public EnumServiceType ServiceType { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.GetHashCode() == this.GetHashCode())
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return (Host + "_" + Port + "_" + (int)ServiceType).GetHashCode();
        }
    }
    /// <summary>
    /// 客户端连接池
    /// </summary>
    public abstract class BaseClientPool
    { }
    /// <summary>
    /// 客户端连接池约定方法
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IClientPool<T>
    {
        /// <summary>
        /// 从对象池取出一个对象
        /// </summary>
        /// <returns></returns>
        T BorrowInstance();

        /// <summary>
        /// 归还一个对象
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="destoryfalg"></param>
        void ReturnInstance(T instance, bool destoryfalg = false);

    }


}
