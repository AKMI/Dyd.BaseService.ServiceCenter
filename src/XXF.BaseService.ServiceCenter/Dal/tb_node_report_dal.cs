using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ServiceCenter.Model;

namespace XXF.BaseService.ServiceCenter.Dal
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
	public partial class tb_node_report_dal
    {
        public virtual bool Add(DbConn PubConn, tb_node_report_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					
					//节点id
					new ProcedureParameter("@nodeid",    model.nodeid),
					//创建时间
					//new ProcedureParameter("@createtime",    model.createtime),
					//错误数量
					new ProcedureParameter("@errorcount",    model.errorcount),
					//当前连接数（访问）数量
					new ProcedureParameter("@connectioncount",    model.connectioncount),
					//访问次数
					new ProcedureParameter("@visitcount",    model.visitcount),
					//线程数
					new ProcedureParameter("@processthreadcount",    model.processthreadcount),
					//
					new ProcedureParameter("@processcpuper",    model.processcpuper),
					//内存大小
					new ProcedureParameter("@memorysize",    model.memorysize)   
                };
            int rev = PubConn.ExecuteSql(string.Format(@"insert into tb_node_report_{0}(nodeid,createtime,errorcount,connectioncount,visitcount,processthreadcount,processcpuper,memorysize)
										   values(@nodeid,getdate(),@errorcount,@connectioncount,@visitcount,@processthreadcount,@processcpuper,@memorysize)",DateTime.Now.ToString("yyyyMMdd")), Par);
            return rev == 1;

        }

        
    }
}