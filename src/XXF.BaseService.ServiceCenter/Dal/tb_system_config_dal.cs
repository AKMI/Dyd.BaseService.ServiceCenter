using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ServiceCenter.Model;

namespace XXF.BaseService.ServiceCenter.Dal
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
	public partial class tb_system_config_dal
    {
        public virtual List<tb_system_config_model> GetAllList(DbConn PubConn)
        {
            List<tb_system_config_model> rs = new List<tb_system_config_model>();
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_system_config s");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
				    rs.Add( CreateModel(dr));
            }
            return rs;
        }

		public virtual tb_system_config_model CreateModel(DataRow dr)
        {
            var o = new tb_system_config_model();
			
			//
			if(dr.Table.Columns.Contains("id"))
			{
				o.id = dr["id"].Toint();
			}
			//
			if(dr.Table.Columns.Contains("key"))
			{
				o.key = dr["key"].Tostring();
			}
			//
			if(dr.Table.Columns.Contains("value"))
			{
				o.value = dr["value"].ToString();
			}
			//
			if(dr.Table.Columns.Contains("remark"))
			{
				o.remark = dr["remark"].ToString();
			}
			return o;
        }
    }
}