using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ServiceCenter.Model
{
    /// <summary>
    /// tb_service Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_service_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 服务名称
        /// </summary>
        public string servicename { get; set; }
        
        /// <summary>
        /// 服务命名空间（服务唯一标示）
        /// </summary>
        public string servicenamespace { get; set; }
        
        /// <summary>
        /// 服务类型, 1=thrift,2=wcf,3=http
        /// </summary>
        public Byte servicetype { get; set; }
        
        /// <summary>
        /// 是否测试
        /// </summary>
        public bool istest { get; set; }
        
        /// <summary>
        /// 是否自动设置服务权重（根据服务性能，ping响应速度）
        /// </summary>
        public bool isautoboostpercent { get; set; }
        
        /// <summary>
        /// 协议版本号（当前）
        /// </summary>
        public double protocolversion { get; set; }
        
        /// <summary>
        /// 协议更新时间
        /// </summary>
        public DateTime protocolversionupdatetime { get; set; }
        
        /// <summary>
        /// 服务变动更新时间（服务节点移除，添加需要更新这个时间）
        /// </summary>
        public DateTime serviceupdatetime { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public String remark { get; set; }
        
    }
}
