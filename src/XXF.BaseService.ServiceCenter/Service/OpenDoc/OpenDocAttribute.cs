﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace XXF.BaseService.ServiceCenter.Service.OpenDoc
{
    /// <summary>
    /// 约定的开源文档特性
    /// </summary>
    public abstract class OpenDocAttribute : System.Attribute
    {
        /// <summary>
        /// 描述
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 详细描述
        /// </summary>
        public string Description { get; set; }

        [JsonIgnore][ScriptIgnore]
        public override object TypeId
        {
            get
            {
                return base.TypeId;
            }
        }

        public OpenDocAttribute() { }

        public OpenDocAttribute(string text, string description)
        {
            Text = text;
            Description = description;
        }
    }
}
