﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service.Provider;
using XXF.BaseService.ServiceCenter.Service.Visitor;
using XXF.BaseService.ServiceCenter.SystemRuntime;

namespace XXF.BaseService.ServiceCenter.Service.Server
{
    /// <summary>
    /// 基础服务类
    /// </summary>
    public abstract class BaseServer : IDisposable
    {
        public virtual EnumServiceType ServiceType { get { return EnumServiceType.None; } }
        public virtual IVisitor ServerVisitor { get; set; }
        //public virtual long CurrentConnectionCount { get { return 0; } }

        public BaseServer()
        {
        }
        /// <summary>
        /// 运行服务（默认运行成功后“中断”）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="port"></param>
        /// <param name="protocal"></param>
        /// <param name="successCallAction"></param>
        public virtual void Run<T>(int port, Protocol.ServiceProtocal protocal, ServiceContext context, Action successCallAction)
        {

        }
        /// <summary>
        /// 关闭服务
        /// </summary>
        public virtual void Stop()
        {

        }

        /// <summary>
        /// 重启服务
        /// </summary>
        public virtual void Restart()
        {

        }
        /// <summary>
        /// 服务释放
        /// </summary>
        public virtual void Dispose()
        {
            this.Stop();
        }
    }
}
