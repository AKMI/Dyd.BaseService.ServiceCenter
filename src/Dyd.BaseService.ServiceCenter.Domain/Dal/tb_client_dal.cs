﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Db;
using Dyd.BaseService.ServiceCenter.Domain.Model;

namespace Dyd.BaseService.ServiceCenter.Domain.Dal
{
    //tb_client
    public partial class tb_client_dal
    {
        #region C

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_client model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into tb_client(");
            strSql.Append("serverid,sessionid,projectname,ip,clientheartbeattime,createtime");
            strSql.Append(") values (");
            strSql.Append("@serverid,@sessionid,@projectname,@ip,@clientheartbeattime,@createtime");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            par.Add(new ProcedureParameter("@serverid", model.serverid));
            par.Add(new ProcedureParameter("@sessionid", model.sessionid));
            par.Add(new ProcedureParameter("@projectname", model.projectname));
            par.Add(new ProcedureParameter("@ip", model.ip));
            par.Add(new ProcedureParameter("@clientheartbeattime", model.clientheartbeattime));
            par.Add(new ProcedureParameter("@createtime", model.createtime));


            object obj = conn.ExecuteSql(strSql.ToString(), par);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_client model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tb_client set ");

            strSql.Append(" serverid = @serverid , ");
            strSql.Append(" sessionid = @sessionid , ");
            strSql.Append(" projectname = @projectname , ");
            strSql.Append(" ip = @ip , ");
            strSql.Append(" clientheartbeattime = @clientheartbeattime , ");
            strSql.Append(" createtime = @createtime  ");
            strSql.Append(" where id=@id ");

            par.Add(new ProcedureParameter("@id", model.id));
            par.Add(new ProcedureParameter("@serverid", model.serverid));
            par.Add(new ProcedureParameter("@sessionid", model.sessionid));
            par.Add(new ProcedureParameter("@projectname", model.projectname));
            par.Add(new ProcedureParameter("@ip", model.ip));
            par.Add(new ProcedureParameter("@clientheartbeattime", model.clientheartbeattime));
            par.Add(new ProcedureParameter("@createtime", model.createtime));

            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tb_client ");
            strSql.Append(" where id=@id");
            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Q
        /// <summary>
        /// 重复规则
        /// </summary>
        public bool IsExists(DbConn conn, tb_client model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_category s where 1=1");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_client Get(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, serverid, sessionid, projectname, ip, clientheartbeattime, createtime  ");
            strSql.Append(" from tb_client ");
            strSql.Append(" where id=@id");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, strSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="search">查询实体</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        public IList<tb_client> GetPageList(DbConn conn, tb_client_search search, out int totalCount)
        {
            totalCount = 0;
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            IList<tb_client> list = new List<tb_client>();
            long startIndex = (search.Pno - 1) * search.PageSize + 1;
            long endIndex = startIndex + search.PageSize - 1;
            par.Add(new ProcedureParameter("startIndex", startIndex));
            par.Add(new ProcedureParameter("endIndex", endIndex));

            StringBuilder baseSql = new StringBuilder();
            string sqlWhere = " where 1=1 ";
            baseSql.Append("select row_number() over(order by id desc) as rownum,");
            #region Query
            if (search.serverid > 0)
            {
                sqlWhere += " and serverid=@serverid";
                par.Add(new ProcedureParameter("serverid", search.serverid));
            }
            #endregion
            baseSql.Append(" * ");
            baseSql.Append(" FROM tb_client with(nolock)");

            string execSql = "select * from(" + baseSql.ToString() + sqlWhere + ")  as s where rownum between @startIndex and @endIndex";
            string countSql = "select count(1) from tb_client with(nolock)" + sqlWhere;
            object obj = conn.ExecuteScalar(countSql, par);
            if (obj != DBNull.Value && obj != null)
            {
                totalCount = LibConvert.ObjToInt(obj);
            }
            DataTable dt = conn.SqlToDataTable(execSql, par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        #endregion

        #region private
        public tb_client CreateModel(DataRow dr)
        {
            var model = new tb_client();
            if (dr.Table.Columns.Contains("id") && dr["id"].ToString() != "")
            {
                model.id = int.Parse(dr["id"].ToString());
            }
            if (dr.Table.Columns.Contains("serverid") && dr["serverid"].ToString() != "")
            {
                model.serverid = int.Parse(dr["serverid"].ToString());
            }
            if (dr.Table.Columns.Contains("sessionid") && dr["sessionid"].ToString() != "")
            {
                model.sessionid = long.Parse(dr["sessionid"].ToString());
            }
            if (dr.Table.Columns.Contains("projectname") && dr["projectname"].ToString() != "")
            {
                model.projectname = dr["projectname"].ToString();
            }
            if (dr.Table.Columns.Contains("ip") && dr["ip"].ToString() != "")
            {
                model.ip = dr["ip"].ToString();
            }
            if (dr.Table.Columns.Contains("clientheartbeattime") && dr["clientheartbeattime"].ToString() != "")
            {
                model.clientheartbeattime = DateTime.Parse(dr["clientheartbeattime"].ToString());
            }
            if (dr.Table.Columns.Contains("createtime") && dr["createtime"].ToString() != "")
            {
                model.createtime = DateTime.Parse(dr["createtime"].ToString());
            }

            return model;
        }
        #endregion
    }
}