﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service.OpenDoc;

namespace Dyd.BaseService.ServiceCenter.Demo.Service
{
    /*
     * 服务仅支持的C#类型:bool;byte;int;int16(short);long;double;string;byte[];List<>;Dictionary<,>;void;class;
     * 非支持类型的服务在本地编译阶段,将报错。所以发布服务前仔细检查下服务及实体的类型。
     * 
     * 注意:公开的方法和服务必须包含[Doc]注释文档属性,否则服务或者方法不会解析成协议。
     */
    [ServiceDoc("我公开的服务名","这里是我公开的服务的描述")]
    public interface IMyService
    {
        [MethodDoc("Mehtod1", "这个方法包含了所支持的所有类型,是一个较好的示例方法", "p1:参数1;p2:参数2;p3:参数3;p4:参数4;p5:参数5;p6:参数6;p7:参数7;p8:参数8;p9:参数9;p10:参数10;p11:参数11;", "返回实体2", "车毅", "2012-8-24", "暂无备注")]
        MyEntity2 Method1(bool p1,byte p2,int p3,Int16 p4,short p5,long p12,double p6,string p7,byte[] p8,List<int> p9,Dictionary<string,string> p10, MyEntity1 p11);
        [MethodDoc("这里是我公开的方法", "我公开的方法描述", "", "返回字符串", "车毅", "2012-8-24", "")]
        string Test();//无参数示例
        [MethodDoc("这里是我公开的方法-Do", "我公开的方法名", "p1:参数1", "", "车毅", "2012-8-24", "")]
        void Do(int p1);//返回 void
    }
}
